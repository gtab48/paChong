import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONPointer;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SplittableRandom;

public class Main {


    public static String count;


    public static void main(String[] args) throws IOException {
        //精选
        String url = "https://www.freebuf.com/fapi/frontend/home/article?page=1&limit=20&type=1&day=&category=%E7%B2%BE%E9%80%89";
        JsonParser(post(url,"JSON"));
        JSONArray data = paging(Integer.parseInt(count));
        saveData("数据",data.toString());
//        System.out.println(data);
    }
    //分页
    public static JSONArray paging(int num) throws IOException {
       JSONArray data = new JSONArray();
        for (int i = 1;i <= num / 20 ;i++){
            String url = "https://www.freebuf.com/fapi/frontend/home/article?page="+i+"&limit=20&type=1&day=&category=%E7%B2%BE%E9%80%89";
            data.put(JsonParser(post(url,"JSON")));
        }
        return data;
    }

    public static void saveData(String fileName,String data){
        BufferedWriter writer = null;
        File file = new File(fileName+".json");
        if(!file.exists()){
            try {
                file.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        //写入
        try{
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,false),"UTF-8"));
            writer.write(data);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                if (writer != null){
                    writer.close();
                }
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        System.out.println("写入成功");
    }

    private  static MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    //获取okHttpClient对象
    private final static OkHttpClient client = new OkHttpClient();

    public static String post(String url, String message) throws IOException {
        RequestBody requestBody = RequestBody.create(JSON,message);
        //创建请求

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)//默认就是GET请求，可以不写
                .build();
        Call call = client.newCall(request);
        okhttp3.Response response = call.execute();
        if(response.isSuccessful()){
            return  response.body().string();
        }else{
            return  "请求出错";
        }
    }
    //JSON 数据处理
    public static JSONArray JsonParser(String text)throws IOException {

        JSONObject object = new JSONObject(text);

        JSONObject data = object.getJSONObject("data");

        JSONArray arr = data.getJSONArray("list");

        count = data.getString("count");

        JSONArray pagingData = new JSONArray();
        for(int i = 0; i < arr.length();i++){
            JSONObject jsonData = new JSONObject();
            JSONObject arrJson = arr.getJSONObject(i);
            jsonData.put("文章标题",arrJson.getString("post_title"));
            jsonData.put("文章地址","www.freebuf.com"+arrJson.getString("url"));
            jsonData.put("发布时间",arrJson.getString("post_date"));
            jsonData.put("发布用户名称",arrJson.getString("username"));
            pagingData.put(jsonData);

        }
//        System.out.println(pagingData);


        return pagingData;
    }


}

