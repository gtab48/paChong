import okhttp3.*;
import okhttp3.OkHttpClient;
import org.json.JSONObject;

import javax.imageio.IIOException;
import java.io.IOException;
import java.io.StringReader;

public class okHttp {
    //json传输方式
    private final static MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    //获取okHttpClient对象
    private final static OkHttpClient client = new OkHttpClient();

    public static String post(String url, String message) throws IOException {
        RequestBody requestBody = RequestBody.create(JSON,message);
        //创建请求
            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)//默认就是GET请求，可以不写
                    .build();
            Call call = client.newCall(request);
            okhttp3.Response response = call.execute();
            if(response.isSuccessful()){
                return  response.body().string();
            }else{
                return  "请求出错";
            }
        }
    //获取limit值
    public static int limit(String url){
        String[] str = url.split("&");
        String limit = str[1];
        String[] num = limit.split("=");
        limit = num[1];
        return Integer.parseInt(limit);
    }

}
